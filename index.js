fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(data => {
      const title = data.map(item => item.title);
      console.log(title);
})


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(`Title: ${json.title}, Status: ${json.completed}`))

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		title: "Complete Task",
		completed: false
	})
})
.then(response => response.json())
.then(data => console.log(data))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		title: "Updated Task",
		completed: true
	})
})
.then(response => response.json())
.then(data => console.log(data))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated Task",
		description: "This task has been updated",
		status: "completed",
		completedAt: "2023-04-26T10:30:00Z",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


